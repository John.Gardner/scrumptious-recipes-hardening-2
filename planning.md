<h1>meal_plans Views</h1>

<h2>MealPlanListView</h2>

* Path: `/meal_plans/`
* show all MealPlans created by user ONLY
* link: CreateView
* each meal plan name link: DetailView

<h2>MealPlanCreateView</h2>

* Path: `/meal_plans/create/`

<h2>MealPlanDetailView</h2>

* `Path: /meal_plans/<int:pk>/`

<h2>MealPlanEditView</h2>

* `Path: /meal_plans/<int:pk>/edit`

<h2>MealPlanDeleteView</h2>

* `Path: /meal_plans/<int:pk>/delete`