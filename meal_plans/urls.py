from django.urls import path

from meal_plans.views import MealPlanListView

urlpatterns = [
    path("meal_plans/", MealPlanListView.as_view(), name="meal_plans_list"),
]
